var path = require('path');
const metroBundler = require('metro-bundler');

var config = {
  extraNodeModules: {
    content: path.resolve(__dirname, 'content')
  },
  getProjectRoots() {
    return [
      // Keep your project directory.
      path.resolve(__dirname)
      // path.resolve(__dirname, './content') // path to the external module
    ];
  }
};
module.exports = config;
