import React, { Component } from 'react';
import Home, { HomeNavigator } from './home';
import Page from './page';

import { DrawerNavigator } from 'react-navigation';
import SideBar from './sidebar';

import { expo } from '../app.json';

const HomeScreenDrawer = DrawerNavigator(
  {
    Home: {
      screen: HomeNavigator,
      navigationOptions: {
        title: expo.name
      }
    },
    Page: {
      screen: Page,
      navigationOptions: {
        title: expo.name
      }
    }
  },
  {
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
    contentComponent: props => <SideBar {...props} />
  }
);

export default HomeScreenDrawer;
