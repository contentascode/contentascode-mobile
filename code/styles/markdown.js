import React from 'react';
import { View, Text, Image } from 'react-native';

import { getUniqueID } from 'react-native-markdown-renderer';
const { StyleSheet } = require('react-native');

const style = StyleSheet.create({
  heading1: {
    fontSize: 14,
    fontFamily: 'Roboto_lightitalic'
  },
  heading2: {
    fontSize: 26,
    fontFamily: 'Roboto_black',
    paddingVertical: 10,
    lineHeight: 28
  },
  heading3: {
    fontSize: 18,
    fontFamily: 'Roboto_bold',
    color: '#6F6F6F',
    paddingVertical: 5
  },
  heading4: {
    fontSize: 16,
    fontFamily: 'Roboto_bold',
    color: '#6F6F6F',
    paddingVertical: 5
  },
  heading5: {
    fontSize: 13,
    fontFamily: 'Roboto_bold',
    color: '#6F6F6F',
    paddingVertical: 5
  },
  heading6: {
    fontSize: 11,
    fontFamily: 'Roboto_bold',
    color: '#6F6F6F',
    paddingVertical: 5
  },
  blockquote: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    margin: 20,
    backgroundColor: '#f8f8f8'
  },
  hr: {
    backgroundColor: '#707070',
    height: 2,
    margin: 5
  },
  text: {
    fontSize: 18,
    fontFamily: 'Roboto_light',
    lineHeight: 22
  },
  strong: {
    fontSize: 18,
    fontFamily: 'Roboto_bold',
    lineHeight: 22
  },
  em: {
    fontSize: 18,
    fontFamily: 'Roboto_lightitalic',
    lineHeight: 22
  }
});

function hasParents(parents, type) {
  return parents.findIndex(el => el.type === type) > -1;
}

const rules = images => ({
  image: (node, children, parent, styles) => {
    if (!images[node.attributes.src]) return;
    // const width = Expo.Asset.fromModule(images[node.attributes.src]).width;
    // const heigh = Expo.Asset.fromModule(images[node.attributes.src]).height;
    return (
      // <View style={{ flex: 1, flexDirection: 'row' }} >
      <Image
        key={getUniqueID()}
        resizeMode="contain"
        style={StyleSheet.flatten([
          // {
          //   flex: 1
          // },
          styles.image
        ])}
        source={images[node.attributes.src]}
      />
      // </View>
    );
  },
  list_item: (node, children, parent, styles) => {
    if (hasParents(parent, 'bullet_list')) {
      return (
        <View key={node.key} style={styles.listUnorderedItem}>
          <Text style={styles.listUnorderedItemIcon}>{'\u25CF'}</Text>
          <View style={[styles.listItem]}>{children}</View>
        </View>
      );
    }

    if (hasParents(parent, 'ordered_list')) {
      return (
        <View key={node.key} style={styles.listOrderedItem}>
          <Text style={styles.listOrderedItemIcon}>{node.index + 1}.</Text>
          <View style={[styles.listItem]}>{children}</View>
        </View>
      );
    }

    return (
      <View key={node.key} style={[styles.listItem]}>
        {children}
      </View>
    );
  }
});

export { style, rules };
