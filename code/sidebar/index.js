import React from 'react';
import { AppRegistry, Image, StatusBar } from 'react-native';
import { Container, Content, Text, List, ListItem } from 'native-base';

import index from '../../content';

const pad = (n, width, z) => {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

// Do it stringly typed for first draft
const sections = Object.keys(index)
  .filter(k => k.indexOf('/') === -1 && index[k].meta.title && k !== 'index.md.js')
  .sort((before, after) => pad(before.split('-')[0], 5) - pad(after.split('-')[0], 5))
  .map(key => ({
    route: 'Page',
    title: index[key].meta.title,
    key: key.replace('/index.md.js', '')
  }));

console.log('sections', sections);
const routes = [{ route: 'Home', title: 'Home', key: {} }, ...sections];

export default class SideBar extends React.Component {
  render() {
    return (
      <Container paddingTop={StatusBar.currentHeight}>
        <Content>
          {/* <Image
            source={{
              uri: 'https://github.com/GeekyAnts/NativeBase-KitchenSink/raw/react-navigation/img/drawer-cover.png'
            }}
            style={{
              height: 120,
              alignSelf: 'stretch',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          />
          <Image
            square
            style={{ height: 80, width: 70 }}
            source={{
              uri: 'https://github.com/GeekyAnts/NativeBase-KitchenSink/raw/react-navigation/img/logo.png'
            }}
          /> */}
          <List
            dataArray={routes}
            renderRow={({ route, title, key }) => {
              return (
                <ListItem button onPress={() => this.props.navigation.navigate(route, { key })}>
                  <Text>{title}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}
