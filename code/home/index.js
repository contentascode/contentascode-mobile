import React, { Component } from 'react';
import { Image, View, StyleSheet } from 'react-native';

import { StackNavigator } from 'react-navigation';

import { compose, withState, withHandlers, pure } from 'recompose';

import { style, rules } from '../styles/markdown';
import styles from './styles';

import Lessons from '../lessons';
import Lesson from '../lesson';

import { Button, Text, Header, Title, Body, Left, Right, Content, List, ListItem, Icon } from 'native-base';

import Markdown from 'react-native-markdown-renderer';

import index from '../../content';

import { expo } from '../../app.json';

const teaser = compose(
  withState('expanded', 'expand', false),
  withHandlers({
    show: ({ expand }) => e => expand(true),
    hide: ({ expand }) => e => expand(false),
    toggle: ({ expand }) => e => expand(current => !current)
  })
);

const { md, images, meta: { title } } = index['index.md.js'];
const SEPARATOR = '<!-- more -->';
const more = md.indexOf(SEPARATOR);
// const INDEX = md.length >= 200 ? 200 : md.length >= 100 ? 100 : 10;
// const line = md.indexOf('\n', INDEX) !== -1 ? md.indexOf('\n', INDEX) : md.indexOf('. ', INDEX);
// const split = more !== -1 ? [more, more + SEPARATOR.length] : [line, line + 2];
const split = more !== -1 ? [more, more + SEPARATOR.length] : [md.length, md.length];

const pad = (n, width, z) => {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

// Do it stringly typed for first draft
const sections = Object.keys(index)
  .filter(k => k.endsWith('/index.md.js') && index[k].meta.title)
  .sort((before, after) => pad(before.split('-')[0], 5) - pad(after.split('-')[0], 5))
  .map(key => ({
    title: index[key].meta.title,
    key: key.replace('/index.md.js', ''),
    page_count: Object.keys(index).filter(
      k => k.startsWith(key.replace('/index.md.js', '')) && !k.endsWith('/index.md.js')
    ).length
  }));

const HomeComponent = ({ navigation, toggle, expanded }) => {
  return (
    <Content>
      <List>
        <ListItem onPress={() => toggle()}>
          <Body>
            <Markdown style={style} rules={rules(images)}>
              {md.slice(0, split[0])}
            </Markdown>
            {md.slice(split[1]).length != 0 &&
              !expanded && <Icon type="Entypo" name="chevron-thin-down" style={{ textAlign: 'center' }} />}
            {md.slice(split[1]).length != 0 &&
              expanded && (
                <Markdown style={style} rules={rules(images)}>
                  {md.slice(split[1])}
                </Markdown>
              )}
            {md.slice(split[1]).length != 0 &&
              expanded && <Icon type="Entypo" name="chevron-thin-up" style={{ textAlign: 'center' }} />}
          </Body>
        </ListItem>
        {sections.map(({ title, key, page_count }) => (
          <ListItem key={key} onPress={() => navigation.navigate('Lessons', { key })}>
            <Body>
              <Text>{title}</Text>
              <Text note>{page_count} sections</Text>
            </Body>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
        ))}
      </List>
    </Content>
  );
};

const Home = compose(teaser, pure)(HomeComponent);

Home.navigationOptions = ({ navigation }) => ({
  title: expo.name,
  headerBackTitle: 'Top',
  // No localisation for now
  // headerRight: (
  //   <Button transparent>
  //     <Icon name="globe" style={{ color: '#060606', marginTop: 12 }} />
  //   </Button>
  // ),
  headerLeft: (
    <Button transparent onPress={() => navigation.navigate('DrawerOpen')}>
      <Icon name="menu" style={{ color: '#060606', marginTop: 12 }} />
    </Button>
  ),
  headerTitleStyle: {
    marginLeft: 0
  }
});

export const HomeNavigator = StackNavigator(
  {
    Home: { screen: Home },
    Lessons: { screen: Lessons },
    Lesson: {
      screen: Lesson,
      navigationOptions: {
        header: null
      }
      //   navigationOptions: props => {
      //     console.log('props', props);
      //     return {
      //       title: index[pages[current].path].meta.title,
      //       headerRight: (
      //         <Button transparent>
      //           <Icon name="globe" style={{ color: '#060606' }} />
      //         </Button>
      //       )
      //     };
      //   }
    }
  },
  {
    headerMode: 'screen',
    cardStyle: {
      backgroundColor: 'white'
    }
  }
);

export default Home;
