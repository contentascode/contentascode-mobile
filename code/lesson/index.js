import React from 'react';
import { StatusBar, Image, View, StyleSheet } from 'react-native';
import { Header, Left, Right, Body, Title, Button, Icon, Container, Content, Text, List, ListItem } from 'native-base';
import { compose, withState, withHandlers, pure } from 'recompose';
import { withNavigation } from 'react-navigation';

import Markdown from 'react-native-markdown-renderer';

import { style, rules } from '../styles/markdown';
import styles from './styles';
import index from '../../content';

const pages = withState('pages', 'setPages', ({ navigation: { state: { params: { pages } } } }) => pages);

const current = compose(
  withState('current', 'setCurrent', ({ navigation: { state: { params: { idx } } } }) => idx),
  withHandlers({
    next: ({ setCurrent, navigation: { state: { params: { pages } } } }) => () =>
      setCurrent(n => (n >= pages.length - 1 ? n : n + 1)),
    previous: ({ setCurrent }) => () => setCurrent(n => (n <= 0 ? n : n - 1))
  })
);

const PreviousNext = ({ previous, next, pages, current }) => (
  <View
    style={{
      flexDirection: 'row',
      flex: 1,
      bottom: 0,
      left: 0,
      right: 0,
      justifyContent: 'space-between',
      padding: 15
    }}>
    {current > 0 ? (
      <Button small iconLeft light onPress={() => previous()}>
        <Icon name="arrow-back" />
        <Text>Previous</Text>
      </Button>
    ) : (
      <View />
    )}
    {current < pages.length - 1 ? (
      <Button small iconRight light onPress={() => next()}>
        <Text>Next</Text>
        <Icon name="arrow-forward" />
      </Button>
    ) : null}
  </View>
);

const LessonComponent = ({ current, previous, next, pages, navigation }) => {
  const { md, images } = index[pages[current].path];
  return (
    <Container style={{ marginTop: StatusBar.currentHeight }}>
      <Header style={styles.header}>
        <Left style={{ flex: 0.15 }}>
          <Button
            transparent
            onPress={() => {
              navigation.goBack();
            }}>
            <Icon
              name="arrow-back"
              style={{
                paddingRight: 5,
                paddingLeft: 5,
                // marginLeft: 0,
                // left: -2,
                // bottom: -2,
                color: '#000'
              }}
            />
            {/* <Text style={{ marginLeft: 0, paddingLeft: 0, left: -2, bottom: -3, color: '#000' }}>Top</Text> */}
          </Button>
        </Left>
        <Body>
          <Title style={styles.title}>{pages[current].title}</Title>
        </Body>
        {/* <Right>
          <Button transparent>
            <Icon name="globe" style={{ color: '#060606' }} />
          </Button>
        </Right> */}
      </Header>
      <Content padder key={current}>
        {current > 0 || current < pages.length - 1 ? (
          <PreviousNext previous={previous} next={next} current={current} pages={pages} />
        ) : null}
        <View>
          <Markdown style={style} rules={rules(images)}>
            {md}
          </Markdown>
        </View>
        {current > 0 || current < pages.length - 1 ? (
          <PreviousNext previous={previous} next={next} current={current} pages={pages} />
        ) : null}
      </Content>
    </Container>
  );
};

const Lesson = compose(pages, current, withNavigation, pure)(LessonComponent);

export default Lesson;
