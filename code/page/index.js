import React from 'react';
import { StatusBar, StyleSheet } from 'react-native';
import { Header, Left, Right, Body, Title, Button, Icon, Container, Content, Text, List, ListItem } from 'native-base';
import Markdown from 'react-native-markdown-renderer';

import { compose, withState, withHandlers, pure } from 'recompose';

import { style, rules } from '../styles/markdown';
import styles from './styles';

import index from '../../content';

const PageComponent = ({ navigation }) => {
  const key = navigation.state.params.key;
  const { md, images } = index[key];
  return (
    <Container style={{ marginTop: StatusBar.currentHeight }}>
      <Header style={styles.header}>
        <Left style={{ flex: 0.15 }}>
          <Button
            transparent
            onPress={() => {
              navigation.goBack();
            }}>
            <Icon
              name="arrow-back"
              style={{
                paddingRight: 5,
                paddingLeft: 5,
                // marginLeft: 0,
                // left: -2,
                // bottom: -2,
                color: '#000'
              }}
            />
            {/* <Text style={{ marginLeft: 0, paddingLeft: 0, left: -2, bottom: -3, color: '#000' }}>Top</Text> */}
          </Button>
        </Left>
        <Body>
          <Title style={styles.title}>{index[key].meta.title}</Title>
        </Body>
        {/* <Right>
          <Button transparent>
            <Icon name="globe" style={{ color: '#060606' }} />
          </Button>
        </Right> */}
      </Header>
      <Content padder>
        <List>
          <ListItem>
            <Body>
              <Markdown style={style} rules={rules(images)}>
                {md}
              </Markdown>
            </Body>
          </ListItem>
        </List>
      </Content>
    </Container>
  );
};

const Page = compose(pure)(PageComponent);

Page.navigationOptions = ({ navigation: { state: { params: { key } } } }) => ({
  title: index[key].meta.title,
  headerBackTitle: 'Top',
  headerTitleStyle: {
    marginLeft: 0
  }
  // headerRight: (
  //   <Button transparent>
  //     <Icon name="globe" style={{ color: '#060606' }} />
  //   </Button>
  // )
});

export default Page;
