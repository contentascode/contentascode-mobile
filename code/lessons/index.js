import React from 'react';
import { StatusBar, StyleSheet } from 'react-native';
import { Header, Left, Right, Button, Icon, Title, Container, Content, Text, List, ListItem, Body } from 'native-base';
import Markdown from 'react-native-markdown-renderer';

import { compose, withState, withHandlers, pure } from 'recompose';

import { style, rules } from '../styles/markdown';
import styles from './styles';

import index from '../../content';

const pad = (n, width, z) => {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

// Do it stringly typed for first draft
const pages = key =>
  Object.keys(index)
    .filter(k => k.startsWith(key) && !k.endsWith('/index.md.js'))
    .sort((before, after) => pad(before.split('-')[0], 5) - pad(after.split('-')[0], 5))
    .map(key => ({
      title: index[key].meta.title,
      path: key
    }));

const teaser = compose(
  withState('expanded', 'expand', false),
  withHandlers({
    show: ({ expand }) => e => expand(true),
    hide: ({ expand }) => e => expand(false),
    toggle: ({ expand }) => e => expand(current => !current)
  })
);

const LessonsComponent = ({ toggle, expanded, navigation: { navigate, state: { params: { key } } } }) => {
  const { md, images } = index[key + '/index.md.js'];
  const SEPARATOR = '<!-- more -->';
  const more = md.indexOf(SEPARATOR);
  // const INDEX = md.length >= 200 ? 200 : md.length >= 100 ? 100 : 10;
  // console.log('INDEX', INDEX);
  // const line = md.indexOf('\n', INDEX) !== -1 ? md.indexOf('\n', INDEX) : md.indexOf('. ', INDEX);
  // console.log('line', line);
  // const split = more !== -1 ? [more, more + SEPARATOR.length] : [line, line + 2];
  const split = more !== -1 ? [more, more + SEPARATOR.length] : [md.length, md.length];
  return (
    <Content>
      <List>
        <ListItem onPress={() => toggle()}>
          <Body>
            <Markdown style={style} rules={rules(images)}>
              {md.slice(0, split[0])}
            </Markdown>
            {md.slice(split[1]).length != 0 &&
              !expanded && <Icon type="Entypo" name="chevron-thin-down" style={{ textAlign: 'center' }} />}
            {md.slice(split[1]).length != 0 &&
              expanded && (
                <Markdown style={style} rules={rules(images)}>
                  {md.slice(split[1])}
                </Markdown>
              )}
            {md.slice(split[1]).length != 0 &&
              expanded && <Icon type="Entypo" name="chevron-thin-up" style={{ textAlign: 'center' }} />}
          </Body>
        </ListItem>
        {pages(key).map(({ title, path }, idx) => (
          <ListItem key={path} onPress={() => navigate('Lesson', { idx, pages: pages(key) })}>
            <Body>
              <Text>{title}</Text>
            </Body>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
        ))}
      </List>
    </Content>
  );
};

const Lessons = compose(teaser, pure)(LessonsComponent);

Lessons.navigationOptions = ({ navigation: { state: { params: { key } } } }) => ({
  title: index[key + '/index.md.js'].meta.title,
  headerBackTitle: 'Top',
  headerTitleStyle: {
    marginLeft: 0
  }
  // headerRight: (
  //   <Button transparent>
  //     <Icon name="globe" style={{ color: '#060606' }} />
  //   </Button>
  // )
});

export default Lessons;
